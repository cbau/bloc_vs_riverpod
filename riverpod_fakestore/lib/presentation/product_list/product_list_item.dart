import 'package:flutter/material.dart';

import '../../data/models/product.dart';

class ProductListItem extends StatelessWidget {
  const ProductListItem({
    required this.item,
    super.key,
  });

  final Product item;

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;

    return Card(
      clipBehavior: Clip.antiAlias,
      child: Column(
        children: [
          Expanded(
            child: SizedBox.expand(
              child: ColoredBox(
                color: Colors.white,
                child: item.image.isEmpty
                    ? const SizedBox()
                    : Image.network(item.image),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Expanded(
                      child: Text(
                        '\$${item.price.toStringAsFixed(2)}',
                        style: textTheme.titleMedium,
                      ),
                    ),
                    Icon(
                      Icons.star,
                      color: textTheme.bodyMedium?.color,
                      size: textTheme.bodyMedium?.fontSize,
                    ),
                    Text(
                      item.rating.rate.toStringAsFixed(1),
                      style: textTheme.bodyMedium,
                    ),
                  ],
                ),
                Text(
                  item.title,
                  maxLines: 1,
                  style: textTheme.bodyMedium,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
