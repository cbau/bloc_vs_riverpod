import 'package:flutter/material.dart';

import 'rating.dart';

@immutable
class Product {
  const Product({
    required this.rating,
    this.id = 0,
    this.title = '',
    this.price = 0.0,
    this.description = '',
    this.category = '',
    this.image = '',
  });

  Product.fromMap(Map<String, Object?>? map)
      : this(
          id: map?['id'] as int? ?? 0,
          title: map?['title'] as String? ?? '',
          price: (map?['price'] as num?)?.toDouble() ?? 0,
          description: map?['description'] as String? ?? '',
          category: map?['category'] as String? ?? '',
          image: map?['image'] as String? ?? '',
          rating: Rating.fromMap(map?['rating'] as Map<String, Object?>?),
        );

  final int id;
  final String title;
  final double price;
  final String description;
  final String category;
  final String image;
  final Rating rating;

  Product copyWith({
    int? id,
    String? title,
    double? price,
    String? description,
    String? category,
    String? image,
    Rating? rating,
  }) =>
      Product(
        id: id ?? this.id,
        title: title ?? this.title,
        price: price ?? this.price,
        description: description ?? this.description,
        category: category ?? this.category,
        image: image ?? this.image,
        rating: rating ?? this.rating,
      );

  Map<String, Object?> toMap() => <String, Object?>{
        'id': id,
        'title': title,
        'price': price,
        'description': description,
        'category': category,
        'image': image,
        'rating': rating.toMap(),
      };

  @override
  String toString() => 'Product(id: $id, title: $title, price: $price, '
      'description: $description, category: $category, image: $image, '
      'rating: $rating)';

  @override
  bool operator ==(covariant Product other) =>
      identical(this, other) ||
      (other.id == id &&
          other.title == title &&
          other.price == price &&
          other.description == description &&
          other.category == category &&
          other.image == image &&
          other.rating == rating);

  @override
  int get hashCode =>
      id.hashCode ^
      title.hashCode ^
      price.hashCode ^
      description.hashCode ^
      category.hashCode ^
      image.hashCode ^
      rating.hashCode;
}
