import 'package:http/http.dart';

class FakeStoreService {
  factory FakeStoreService() => _instance;

  const FakeStoreService._();

  static const _instance = FakeStoreService._();

  String get serviceOrigin => 'https://fakestoreapi.com';

  Future<Response> getProducts() => get(Uri.parse('$serviceOrigin/products'));

  Future<Response> getProduct(int id) =>
      get(Uri.parse('$serviceOrigin/products/$id'));
}
