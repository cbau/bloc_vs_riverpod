import 'dart:async';

import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../data/models/product.dart';
import '../../data/repositories/product_repository.dart';
import 'product_list_state.dart';

final productListProvider =
    StateNotifierProvider<ProductListNotifier, ProductListState>(
  (ref) => ProductListNotifier(),
);

class ProductListNotifier extends StateNotifier<ProductListState> {
  ProductListNotifier() : super(const ProductListStateLoading()) {
    unawaited(_init());
  }

  String query = '';
  List<Product> items = <Product>[];

  Future<void> _init() async {
    try {
      items = await ProductRepository().all;
      _setFilteredItems();
    } on Exception catch (e) {
      state = ProductListStateError(error: e.toString());
    }
  }

  void onSearchChanged(String? query) {
    this.query = query?.toLowerCase() ?? '';
    _setFilteredItems();
  }

  void _setFilteredItems() {
    if (query.isEmpty) {
      state = ProductListStateSuccess(items: items);
    } else {
      final filteredItems =
          items.where((e) => e.title.toLowerCase().contains(query));
      if (filteredItems.isEmpty) {
        state =
            ProductListStateError(error: 'No items found for query "$query".');
      } else {
        state = ProductListStateSuccess(items: filteredItems.toList());
      }
    }
  }
}
