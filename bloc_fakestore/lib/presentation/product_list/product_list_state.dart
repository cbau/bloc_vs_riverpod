import '../../data/models/product.dart';

sealed class ProductListState {
  const ProductListState();
}

class ProductListStateLoading extends ProductListState {
  const ProductListStateLoading();
}

class ProductListStateError extends ProductListState {
  const ProductListStateError({
    required this.error,
  });

  final String error;
}

class ProductListStateSuccess extends ProductListState {
  const ProductListStateSuccess({
    required this.items,
  });

  final List<Product> items;
}
