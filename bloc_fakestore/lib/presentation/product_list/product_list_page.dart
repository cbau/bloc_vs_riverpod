import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../common/message_dialog.dart';
import 'product_list_bloc.dart';
import 'product_list_item.dart';
import 'product_list_state.dart';

class ProductListPage extends StatelessWidget {
  const ProductListPage({super.key});

  @override
  Widget build(BuildContext context) =>
      BlocBuilder<ProductListBloc, ProductListState>(
        builder: (context, state) =>
            BlocListener<ProductListBloc, ProductListState>(
          listener: (context, state) async {
            if (state is ProductListStateError) {
              await showDialog<void>(
                context: context,
                builder: (context) => MessageDialog(
                  content: Text(state.error),
                ),
              );
            }
          },
          listenWhen: (previous, current) => current is ProductListStateError,
          child: Scaffold(
            appBar: AppBar(
              title: const Text('Fake Store (with Bloc)'),
              bottom: PreferredSize(
                preferredSize: const Size.fromHeight(40),
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 16,
                  ),
                  child: TextFormField(
                    decoration: const InputDecoration(
                      hintText: 'Search',
                      prefixIcon: Icon(Icons.search),
                    ),
                    onChanged: BlocProvider.of<ProductListBloc>(context)
                        .onSearchChanged,
                  ),
                ),
              ),
            ),
            body: switch (state) {
              ProductListStateError() => Center(
                  child: Text(state.error),
                ),
              ProductListStateLoading() => const Center(
                  child: CircularProgressIndicator(),
                ),
              ProductListStateSuccess() => GridView.builder(
                  gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
                    maxCrossAxisExtent: 180,
                    mainAxisExtent: 240,
                  ),
                  itemBuilder: (context, index) =>
                      ProductListItem(item: state.items[index]),
                  itemCount: state.items.length,
                  padding: const EdgeInsets.all(16),
                ),
            },
          ),
        ),
      );
}
