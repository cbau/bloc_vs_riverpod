# Bloc vs Riverpod

A small project to compare the similitudes and differences between the state managers Bloc and Riverpod.

## Setting the application provider

Both state manages create a provider for the whole application with a widget that wraps the application widget (usually `materialApp`). In Bloc this widget is called `MultiBlocProvider`, and in Riverpod it's called `ProviderScope`.

| Bloc | Riverpod |
| - | - |
| ![MultiBlocProvider](assets/bloc_set_provider.png "MultiBlocProvider") | ![ProviderScope](assets/riverpod_set_provider.png "ProviderScope") |
| Ref: [main_app.dart, L12](https://gitlab.com/cbau/bloc_vs_riverpod/-/blob/main/bloc_fakestore/lib/presentation/main_app.dart?ref_type=heads#L12) | Ref: [main_app.dart, L11](https://gitlab.com/cbau/bloc_vs_riverpod/-/blob/main/riverpod_fakestore/lib/presentation/main_app.dart?ref_type=heads#L11) |

This code might seem simpler on Riverpod, which would give it a point, but…

## Providing the bloc/notifier

As seen before, to add a provider with Bloc, there's a property `providers` inside the `MultiBlocProvider`, where you can create a `BlocProvider` for each of your Blocs that you want to provide through the application.

On the other hand, to add a provider with Riverpod, you simple create a `StateNotifierProvider` assigned to a value… anywhere out of any class. The standard is to add it in the same file as the notifier, but since there's nothing that prevents you to create it anywhere else (which can lead to trash code by some programmers), I'll give this point to Bloc.

| Bloc | Riverpod |
| - | - |
| ![BlocProvider](assets/bloc_add_provider.png "BlocProvider") | ![StateNotifierProvider](assets/riverpod_add_provider.png "StateNotifierProvider") |
| Ref: [main_app.dart, L14](https://gitlab.com/cbau/bloc_vs_riverpod/-/blob/main/bloc_fakestore/lib/presentation/main_app.dart?ref_type=heads#L14) | Ref: [product_list_notifier.dart, L9](https://gitlab.com/cbau/bloc_vs_riverpod/-/blob/main/riverpod_fakestore/lib/presentation/product_list/product_list_notifier.dart?ref_type=heads#L9) |

## Creating the bloc/notifier

The logic class are created in similar ways in both state managers, even if by standard they have different names (bloc for Bloc, and notifier for Riverpod). These are created extending a `Cubit` in Bloc\*, and a `StateNotifier` in Riverpod. They both implement a state, which is exactly the same for both state manages, and they both also receive a default state in their super constructor.

| Bloc | Riverpod |
| - | - |
| ![Cubit](assets/bloc_create.png "Cubit") | ![StateNotifier](assets/riverpod_create.png "StateNotifier") |
| Ref: [product_list_bloc.dart, L9](https://gitlab.com/cbau/bloc_vs_riverpod/-/blob/main/bloc_fakestore/lib/presentation/product_list/product_list_bloc.dart?ref_type=heads#L9) | Ref: [product_list_notifier.dart, L14](https://gitlab.com/cbau/bloc_vs_riverpod/-/blob/main/riverpod_fakestore/lib/presentation/product_list/product_list_notifier.dart?ref_type=heads#L14) |

In Bloc the function `emit` is used to update the state, while in Riverpod you directly assign the new `state` to a variable of the same name, both behaving the same way when updated such state.

| Bloc | Riverpod |
| - | - |
| ![emit](assets/bloc_set_state.png "emit") | ![state =](assets/riverpod_set_state.png "state =") |
| Ref: [product_list_bloc.dart, L33](https://gitlab.com/cbau/bloc_vs_riverpod/-/blob/main/bloc_fakestore/lib/presentation/product_list/product_list_bloc.dart?ref_type=heads#L33) | Ref: [product_list_notifier.dart, L38](https://gitlab.com/cbau/bloc_vs_riverpod/-/blob/main/riverpod_fakestore/lib/presentation/product_list/product_list_notifier.dart?ref_type=heads#L38) |

Since both are the same here, both receive a point.

\* You can also extend a `Bloc` instead of a `Cubit`, which implements an event as well as the state, but this approach will be out of the scope of this comparison, since Riverpod doesn't have an equivalent for this option.

## Building widgets from state changes

To build widgets from the state changes, Bloc uses a `BlocBuilder` that provides a context and a state, and Riverpod uses a `Consumer` that provides a context, a reference and an optional child. Since Riverpod doesn't provides the state directly, you need to set it using `ref.watch(provider)` pointing to the value of the notifier provider created before.

| Bloc | Riverpod |
| - | - |
| ![BlocBuilder](assets/bloc_consume.png "BlocBuilder") | ![Consumer and ref.watch](assets/riverpod_consume.png "Consumer and ref.watch") |
| Ref: [product_list_page.dart, L14](https://gitlab.com/cbau/bloc_vs_riverpod/-/blob/main/bloc_fakestore/lib/presentation/product_list/product_list_page.dart?ref_type=heads#L14) | Ref: [product_list_page.dart, L13](https://gitlab.com/cbau/bloc_vs_riverpod/-/blob/main/riverpod_fakestore/lib/presentation/product_list/product_list_page.dart?ref_type=heads#L13) |

It's more common to see Riverpod users using a `ConsumerWidget` instead of `Consumer` inside a `StatelessWidget`. They are both the same case, only that one follows the standard Flutter widgets, and the other used their custom standard instead. In the end, it's up to you to choose which one to use. For this exampled, I preferred using `Consumer` as it's easier to see the similitude with `BlocBuilder` here.

You might think that `BlocBuilder` is more complex here because you have to specify the bloc and the state we are going to use, but if you remember well, we did the same for the `StateNotifierProvider` before when we created it, while we didn't do it for the `BlocProvider`. So in the end, they are both the same on different places.

Technically we don't need the `BlocBuilder` wrapping the whole widget, since it's used only on the scaffold body, but I left it here to better compare the similitudes between the two state managers, as the `Consumer` on the other hand need to wrap all the scaffold, since it's used as well for calls to the notifier and the listener, as we are going to see later below.

Using the state is the same in both cases, as seen in the body for Bloc in [product_list_page.dart, L48](https://gitlab.com/cbau/bloc_vs_riverpod/-/blob/main/bloc_fakestore/lib/presentation/product_list/product_list_page.dart?ref_type=heads#L48) and for Riverpod in [product_list_page.dart, L46](https://gitlab.com/cbau/bloc_vs_riverpod/-/blob/main/riverpod_fakestore/lib/presentation/product_list/product_list_page.dart?ref_type=heads#L46).

Since they are both similar, with their own optimization oportunities, I'll give a point to both.

`BlocBuilder` will automatically get a bloc from your `MultiBlocProvider` but you can instead create and assign a `Bloc` directly in your `BlocBuilder`, without adding it to your `MultiBlocProvider`, which is very practical for some cases, like a details page. This is something Riverpod can't do, so I'll give Bloc an extra point for this.

## Calling a function in the bloc/notifier

In Bloc you access the bloc with `BlocProvider.of<Type>(context)`, specifying the bloc type you want to access. In Riverpod you access the notifier with `ref.read(provider.notifier)`, specifying the provider value you want to access. Please note that you must read the notifier of the provider, and not the provider itself, or this will fail to retrieve the value you need. Once you access the bloc/notifier, you simply call the function you need from it, or like in the example, assign the function to a callback property:

| Bloc | Riverpod |
| - | - |
| ![BlocProvider.of](assets/bloc_call.png "BlocProvider.of") | ![Consumer and ref.read](assets/riverpod_call.png "Consumer and ref.read") |
| Ref: [product_list_page.dart, L42](https://gitlab.com/cbau/bloc_vs_riverpod/-/blob/main/bloc_fakestore/lib/presentation/product_list/product_list_page.dart?ref_type=heads#L42) | Ref: [product_list_page.dart, L40](https://gitlab.com/cbau/bloc_vs_riverpod/-/blob/main/riverpod_fakestore/lib/presentation/product_list/product_list_page.dart?ref_type=heads#L40) |

Since they are both moreless the same, I would give a point to both here as well, but since I've seen many programmers confused by reading the provider instead of it's notifier on Riverpod, I'll give this point only to Bloc.

Also, `ref.read` must live inside a `Consumer`, while `BlocProvider.of` can be accessed out of a `BlocBuilder`, as long as it's a descendant of the `MultiBlocProvider` containing the `BlocProvider`.

## Listening to state changes

To listen to changes for triggering actions instead of rebuilding the widgets, Bloc has `BlocListener`, which receives a `listener` callback to process and trigger the actions. Riverpod on the other hand has `ref.listen`, which receives the provider and the callback to process and trigger the actions.

| Bloc | Riverpod |
| - | - |
| ![BlocListener](assets/bloc_listen.png "BlocListener") | ![ref.listen](assets/riverpod_listen.png "Consumer and ref.listen") |
| Ref: [product_list_page.dart, L16](https://gitlab.com/cbau/bloc_vs_riverpod/-/blob/main/bloc_fakestore/lib/presentation/product_list/product_list_page.dart?ref_type=heads#L16) | Ref: [product_list_page.dart, L16](https://gitlab.com/cbau/bloc_vs_riverpod/-/blob/main/riverpod_fakestore/lib/presentation/product_list/product_list_page.dart?ref_type=heads#L16) |

`BlocListener` has a property `listenWhen`, which allows to optimize the calls to `listener` considering the properties of the previous and current state, while Riverpod doesn't optimize these calls ans simply send the previous and next states directly on the listener callback to process them as needed. `BlocBuilder` also has a property `buildWhen` to optimize the build callbacks if needed.

Even without this optimizations, Riverpod seems a little simpler here since `BlocListener` requires to specify again the bloc and state, just as when we created `BlocBuilder`, but there's a simpler option using `BlocConsumer` to create a `builder` and a `listener` on the same bloc with just one widget. We just had them separate on this example to better compare the similitudes betweern Bloc and Riverpod.

Since they both have their pros and cons on this topic, I'll set it as a tie and give a point to both of them.

The older popular state manager "Provider" doesn't have this functionality. Riverpod originally was created as the second version of Provider, but since it became too different from the original product, it was released as a new option instead. Because of this, many don't consider Provider as an option nowadays, and this is also the reason why it wasn't considered for this comparison.

## Conclusion

With 6 points for Bloc against the 3 points for Riverpod, I would recommend Bloc as a state manager. Of course, this is only my personal opinion, and this comparison is just the tip of the iceberg. There is much more to see with both state managers after this quick tour.

I just hope that this comparison will help you to see the differences between the two of them, so you can make your own decision. In the end, they are both great options for your personal and professional projects.

And the best state manager in the end is the one that works best for you and your project.
