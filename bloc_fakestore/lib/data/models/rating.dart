import 'package:flutter/material.dart';

@immutable
class Rating {
  const Rating({
    this.rate = 0.0,
    this.count = 0,
  });

  Rating.fromMap(Map<String, Object?>? map)
      : this(
          rate: (map?['rate'] as num?)?.toDouble() ?? 0,
          count: map?['count'] as int? ?? 0,
        );

  final double rate;
  final int count;

  Rating copyWith({
    double? rate,
    int? count,
  }) =>
      Rating(
        rate: rate ?? this.rate,
        count: count ?? this.count,
      );

  Map<String, Object?> toMap() => <String, Object?>{
        'rate': rate,
        'count': count,
      };

  @override
  String toString() => 'Rating(rate: $rate, count: $count)';

  @override
  bool operator ==(covariant Rating other) =>
      identical(this, other) || (other.rate == rate && other.count == count);

  @override
  int get hashCode => rate.hashCode ^ count.hashCode;
}
