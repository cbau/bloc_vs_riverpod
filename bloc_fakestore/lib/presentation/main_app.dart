import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'app_theme.dart';
import 'product_list/product_list_bloc.dart';
import 'product_list/product_list_page.dart';

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) => MultiBlocProvider(
        providers: [
          BlocProvider(
            create: (context) => ProductListBloc(),
          ),
        ],
        child: MaterialApp(
          darkTheme: AppTheme().dark,
          home: const ProductListPage(),
          theme: AppTheme().light,
          //themeMode: ThemeMode.dark,
          title: 'Fake Store (with Bloc)',
        ),
      );
}
