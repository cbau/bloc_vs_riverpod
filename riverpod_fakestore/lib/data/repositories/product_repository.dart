import 'dart:convert';

import '../models/product.dart';
import '../services/fake_store_service.dart';

class ProductRepository {
  factory ProductRepository() => _instance;

  const ProductRepository._();

  static const _instance = ProductRepository._();

  Future<List<Product>> get all async {
    final response = await FakeStoreService().getProducts();

    if (response.statusCode >= 300) {
      throw Exception('${response.statusCode} ${response.reasonPhrase}');
    }

    final list = json.decode(response.body) as List;
    final items =
        list.map((e) => Product.fromMap(e as Map<String, Object?>?)).toList();

    return items;
  }

  Future<Product?> get(int id) async {
    final response = await FakeStoreService().getProduct(id);

    if (response.statusCode >= 300) {
      throw Exception('${response.statusCode} ${response.reasonPhrase}');
    }

    final map = json.decode(response.body) as Map<String, Object?>?;
    return Product.fromMap(map);
  }
}
