# Riverpod Fakestore

A fake store implementation with Riverpod as the state manager.

## Demo

See this project working on web at this link:

[WebApp](https://cbau.gitlab.io/bloc_vs_riverpod/riverpod_fakestore/)

## Compile

To run this project on your device, run this command:

```bash
flutter run --release
```
