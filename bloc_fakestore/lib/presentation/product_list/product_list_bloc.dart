import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';

import '../../data/models/product.dart';
import '../../data/repositories/product_repository.dart';
import 'product_list_state.dart';

class ProductListBloc extends Cubit<ProductListState> {
  ProductListBloc() : super(const ProductListStateLoading()) {
    unawaited(_init());
  }

  String query = '';
  List<Product> items = <Product>[];

  Future<void> _init() async {
    try {
      items = await ProductRepository().all;
      _setFilteredItems();
    } on Exception catch (e) {
      emit(ProductListStateError(error: e.toString()));
    }
  }

  void onSearchChanged(String? query) {
    this.query = query?.toLowerCase() ?? '';
    _setFilteredItems();
  }

  void _setFilteredItems() {
    if (query.isEmpty) {
      emit(ProductListStateSuccess(items: items));
    } else {
      final filteredItems =
          items.where((e) => e.title.toLowerCase().contains(query));
      if (filteredItems.isEmpty) {
        emit(
          ProductListStateError(error: 'No items found for query "$query".'),
        );
      } else {
        emit(ProductListStateSuccess(items: filteredItems.toList()));
      }
    }
  }
}
